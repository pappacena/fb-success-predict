# -*- coding: utf-8 -*-
import pandas
import nltk.corpus
nltk.download('stopwords')
from nltk.stem.snowball import PortugueseStemmer
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.preprocessing import StandardScaler, MinMaxScaler
from sklearn.pipeline import Pipeline
from sklearn.linear_model import SGDClassifier, LinearRegression
from sklearn.neural_network import MLPRegressor
from sklearn.naive_bayes import MultinomialNB
from sklearn.cross_validation import train_test_split
from sklearn.decomposition import TruncatedSVD
from sklearn.metrics import mean_squared_error
import matplotlib.pyplot as plt
import numpy as np
import re

def debug(func):
    def d(*args):
        ret = func(*args)
        input("\n\n======\nargs=%s\nret=%s" % (args, ret))
        return ret
    return d

def clean_text(text):
    # remove links
    text = re.sub(r'https?:\/\/.*[\w]*', '', text, flags=re.MULTILINE)
    
    text = "".join(i for i in text if i.isalpha() or i == " ")
    
    # remove stopwords
    text = [i.lower() for i in text.split()
            if i not in nltk.corpus.stopwords.words('portuguese')]
    
    # stemmer
    stemmer = PortugueseStemmer()
    text = [stemmer.stem(w) for w in text]
    return " ".join(text)
    

class StemmedCountVectorizer(CountVectorizer):
    def build_analyzer(self):
        analyzer = super(CountVectorizer, self).build_analyzer()
        stemmer = PortugueseStemmer()
        return lambda doc: (stemmer.stem(w) for w in analyzer(doc))


class DenseTfidfTransformer(TfidfTransformer):    
    def transform(self, *args, **kwargs):
        return super(DenseTfidfTransformer, self).transform(*args, **kwargs).toarray()
    

dataset = pandas.read_csv("dataset.csv", delimiter="\t", quoting=False, index_col=False)
X = [clean_text("%s %s" % (i[1], i[2]))
     for i in dataset.iloc[:, [0, 1]].replace(np.nan, "").to_records()]
# Y = dataset.iloc[:, 2:-1].replace(np.nan, 0).sum(1)  # reactions
Y = dataset.iloc[:, -1] # shares

Y = np.clip(Y, 0, int(np.percentile(Y, 95))) # Remove outliers

X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=0.1, random_state=0)


vectorizer = StemmedCountVectorizer(
    max_features=3000,
    stop_words=nltk.corpus.stopwords.words('portuguese')
)
vectorized = vectorizer.fit_transform(X_train)

pipeline = Pipeline([
    ('vect', CountVectorizer(max_features=3000, binary=True)),
    ('tfidf', DenseTfidfTransformer()),
    # ('clf', MultinomialNB()),
    # ('scale', MinMaxScaler(feature_range=(-1, 1))),
    ('nn', MLPRegressor(activation="relu", max_iter=1500, verbose=True))
])

# Find the best
from sklearn.model_selection import GridSearchCV
parameters = {
    'vect__ngram_range': [(1, 2), (1, 3)],
    # 'tfidf__use_idf': (True, False),
    # 'clf__alpha': (1e-2, 1e-3),
    'nn__hidden_layer_sizes': [(200, 200, 200, 200, 200)]
}
best_model_searcher = GridSearchCV(pipeline, parameters, n_jobs=2)

model = best_model_searcher.fit(X_train, Y_train)
predicted = model.predict(X_test)

diff = mean_squared_error(Y_test, predicted) ** .5
print("Average error: %.1f" % (diff))

to_plot_pred = predicted[0:30]
to_plot_y = Y_test[0:30]
plt.scatter(range(len(to_plot_pred )), to_plot_pred , color="red")
plt.scatter(range(len(to_plot_y)), to_plot_y , color="blue")
plt.show()

plt.hist(abs(Y_test - predicted) / Y_test * 100, bins=50, range=(0, 150))
plt.show()
plt.hist(Y_test, bins=50, range=(0, 2000))
plt.show()