# -*- coding: utf-8 -*-
import facebook
import math
import datetime
import pytz

token = "EAACEdEose0cBAHuQYtwIiZAR9kkipzgduHxS1rUqWBb191ZBLQsUV3ZCodl9ZBh1ZBqo3Cy8N0bD9KNVzBv2ttK9D2JSe0pSNXLXZA6vJMw190uuPDp36AVpEWuYxzVO0pxo8gVTKPJCMzSoB0nqvi8VVyv8dgqzmNJFltRV2yTY63MCRWuloSd0CRdT3zOe8ZD"
page_name = "g1"
total_posts = 3500
MIN_DATE = datetime.datetime(2016, 1, 1, tzinfo=pytz.utc)

reactions = """reactions.type(LIKE).limit(0).summary(1).as(like),
reactions.type(LOVE).limit(0).summary(1).as(love),
reactions.type(HAHA).limit(0).summary(1).as(haha),
reactions.type(WOW).limit(0).summary(1).as(wow),
reactions.type(SAD).limit(0).summary(1).as(sad),
reactions.type(ANGRY).limit(0).summary(1).as(angry)"""
fields = "message,attachments,shares,created_time,%s" % reactions.replace("\n", "")

fb = facebook.GraphAPI(token, version="2.10")

def to_cell(text):
    return text.replace("\t", " ").replace("\n", ";").replace('"', "'")

dataset = []
after = None
total = 0
for i in range(math.ceil(total_posts / 100)):
    posts = fb.get_connections(page_name, "feed", fields=fields, limit=100, after=after)
    after = posts['paging']['cursors']['after']
    for post in posts["data"]:
        created_at = datetime.datetime.strptime(post["created_time"], "%Y-%m-%dT%H:%M:%S%z")
        if created_at < MIN_DATE:
            break
        total += 1
        print("Got %s posts" % total)
        msg = post.get("message", "")
        attachments = post.get("attachments", {}).get("data", [])
        if len(attachments):
            first_attachment_title = attachments[0].get("title", "")
        else:
            first_attachment_title = ""
        
        like = post["like"]["summary"]["total_count"] or 0
        love = post["love"]["summary"]["total_count"] or 0
        haha = post["haha"]["summary"]["total_count"] or 0
        wow = post["wow"]["summary"]["total_count"] or 0
        sad = post["sad"]["summary"]["total_count"] or 0
        angry = post["angry"]["summary"]["total_count"] or 0
        reactions = like + love + haha + wow + sad + angry
        shares = post.get("shares", {}).get("count", 0)
        
        dataset.append([
            to_cell(msg), to_cell(first_attachment_title),
            reactions, like, love, haha, wow, sad, angry,
            shares
        ])

fd = open("dataset.csv", "w", encoding="utf8")
fd.write("\t".join([
    "msg", "first_attachment_title",
    "reactions", "like", "love", "haha", "wow", "sad", "angry",
    "shares"
]))
fd.write("\n")
for data in dataset:
    try:
        fd.write("\t".join(str(i) for i in data))
        fd.write("\n")
    except Exception as e:
        input("%s %s" % (e, data))
fd.close()